# roadrunner

## requeriments

* puppet 2.7+
* puppetlabs vcsrepo

## how to use

```
class { 'roadrunner': 
  email 	=> 'youremail@domain.tld',
  csr_path 	=> '/etc/x509/requests/domain.csr',
  webroot 	=> '/var/www',
  webserver => 'nginx'
}
```
## what needs to be done

* check variables values
* clean unused stuff
* the abolition of prisons