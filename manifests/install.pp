#
class roadrunner::install inherits roadrunner {

  ensure_packages(['openssl'])

  group { $group:
    ensure    => 'present',
    allowdupe => false,
  }

  user { $user:
    ensure    => 'present',
    allowdupe => false,
    gid       => $user,
    require   => Group[$user],
  }

  file { $cert_path:
    ensure => 'directory',
    owner  => 'root',
    mode   => '0750'
  }

  file { $acme_conf_dir:
    ensure => 'directory',
    owner  => 'root'
  }

  file { "${acme_conf_dir}/account.conf":
    ensure  => 'present',
    content => template('roadrunner/account.conf.erb'),
  }

  exec { 'acme-generate-account-key':
    command => "/usr/bin/openssl genrsa 4096 > ${acme_conf_dir}/account.key",
    cwd     => $acme_conf_dir,
    onlyif  => "/usr/bin/test ! -f ${acme_conf_dir}/account.key"
  }

  file { "${acme_conf_dir}/account.key":
    ensure  => present,
    require => Exec['acme-generate-account-key'],
  }

  if $acmesh_install {

    vcsrepo { $acmesh_install_path:
      ensure   => present,
      provider => git,
      source   => $acmesh_repository,
      revision => $acmesh_version,
      owner    => $user,
      group    => $group,
      require  => [ User[$user], Group[$group] ]
    }
  }

  if $dhparams_generation {
    file { $dhparams_directory:
      ensure => 'directory'
    }

    exec { 'acme-generate-account-key':
      command => "/usr/bin/openssl genrsa 4096 > ${acme_conf_dir}/account.key",
      cwd     => $acme_conf_dir,
      onlyif  => "/usr/bin/test ! -f ${acme_conf_dir}/account.key"
    }
  }

  file { "${acme_conf_dir}/renewal.sh":
    ensure  => 'present',
    content => template('roadrunner/cron.erb'),
    mode    => '0755'
  }

}