# certificate configuration
class roadrunner::config inherits roadrunner {

  exec { 'sign_certificate':
    command => '/etc/x509/acmeconf/renewal.sh',
    user    => 'root',
    onlyif  => "/usr/bin/test ! -f ${cert_path}/*/fullchain.cer"
  }

  cron { 'renew_certificate':
    command => '/etc/x509/acmeconf/renewal.sh',
    user    => 'root',
    hour    => '*/8'
  }

}