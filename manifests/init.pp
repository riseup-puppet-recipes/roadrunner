#
class roadrunner (
    $csr_path            = $roadrunner::params::csr_path,
    $email               = $roadrunner::params::email,
    $webroot             = $roadrunner::params::webroot,
    $webserver           = $roadrunner::params::webserver,
    $cert_path           = $roadrunner::params::cert_path,
    $acme_conf_dir       = $roadrunner::params::acme_conf_dir,
    $account_key         = $roadrunner::params::account_key,
    $user                = $roadrunner::params::user,
    $group               = $roadrunner::params::group,
    $acme_server         = $roadrunner::params::acme_server,
    $acmesh_install      = $roadrunner::params::acmesh_install,
    $acmesh_repository   = $roadrunner::params::acmesh_repository,
    $acmesh_version      = $roadrunner::params::acmesh_version,
    $acmesh_install_path = $roadrunner::params::acmesh_install_path,
    $service_enable      = $roadrunner::params::service_enable,
    $service_ensure      = $roadrunner::params::service_ensure,
    $service_manage      = $roadrunner::params::service_manage,
) inherits roadrunner::params {

    # validations here


    # exec
    anchor { 'roadrunner:begin': } ->
    class { '::roadrunner::install': } ->
    class { '::roadrunner::config': } ->
    #class { '::roadrunner::service': } ->
    anchor { 'roadrunner::end': }

}
