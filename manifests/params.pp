#
class roadrunner::params {
    $csr_path               = ''
    $email                  = ''
    $webroot                = ''
    $webserver              = ''

    $cert_path              = '/etc/x509/certificates'
    $acme_conf_dir          = '/etc/x509/acmeconf'

    $user                   = 'roadrunner'
    $group                  = 'roadrunner'
    $dhparams_generation    = false


    $acmesh_install         = true
    $acmesh_repository      = 'https://github.com/Neilpang/acme.sh.git'
    $acmesh_version         = 'master'
    $acmesh_install_path    = '/opt/acme.sh'

    $acmesh_home            = '/etc/x509/'
    $acmesh_account_key     = '/etc/x509/account.key'
    $acmesh_certificates    = '/etc/x509/certs/'
    $acmesh_email           = ''


    $service_enable         = true
    $service_ensure         = running
    $service_manage         = true
}