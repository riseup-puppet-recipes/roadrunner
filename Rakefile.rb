require 'puppet-lint/tasks/puppet-lint'
PuppetLint.configuration.send("disable_variable_scope")

task :default => [:lint]
